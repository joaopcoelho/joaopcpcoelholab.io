---
layout: page
title: Now
permalink: /now/
---

I'm living in Stockholm and working as a data scientist in digital marketing.

Over the past 14.5 weeks my main focus has been on creating a consistent learning habit for myself. As a result I've been able to consistently dedicate around 20 hours per week to my own projects, which is huge compared to the previous total of 0. I recently completed my project of learning about [markov chains]({% post_url 2018-05-06-markov-chains %}) and am considering what to take on next.

Inspired by [Derek Sivers](https://sivers.org/now) and the now page [movement](https://nownownow.com/about).

Last update on 10 may 2018.